package com.jspider.jsondatahandling;

/**
 * Created by chandan on 4/23/2017.
 */

public class Model {

    private String itemName;
    private String itemDescription;

    public Model(String itemName, String description) {
        this.itemName =itemName;
        this.itemDescription = description;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }


}
