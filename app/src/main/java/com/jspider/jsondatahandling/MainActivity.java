package com.jspider.jsondatahandling;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private String TAG = MainActivity.class.getSimpleName();
    private ProgressDialog pDialog;
    ListView lv;
    LinearLayout linlaHeaderProgress;
    ArrayList<Model> contactList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv = (ListView) findViewById(R.id.lv);
        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
        contactList = new ArrayList<>();
        new GetContact().execute();
    }

    private class GetContact extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            linlaHeaderProgress.setVisibility(View.VISIBLE);
            Toast.makeText(MainActivity.this, "Json Data is downloading", Toast.LENGTH_LONG).show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpHandler hh = new HttpHandler();
            // Making a request to url and getting response
            String url = "http://api.androidhive.info/json/movies.json";

            String jsonStr = hh.makeServiceCall(url);
            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONArray jsonArray=new JSONArray(jsonStr);



                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject c = jsonArray.getJSONObject(i);
                        String title = c.getString("title");
                        String rating = c.getString("rating");




                        // tmp hash map for single contact


                        // adding each child node to HashMap key => value


                        // adding contact to contact list
                        contactList.add(new Model(title,rating));
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                }

            } else {
                Log.e(TAG, "Couldn't get json from server.");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Adapter adapter = new Adapter(MainActivity.this, contactList);
            lv.setAdapter(adapter);
            linlaHeaderProgress.setVisibility(View.GONE);

        }


    }
}
