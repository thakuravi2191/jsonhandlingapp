package com.jspider.jsondatahandling;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by chandan on 4/23/2017.
 */

public class Adapter extends BaseAdapter {
    private Context context;
    public ArrayList<Model> items;

    public Adapter(Context context, ArrayList<Model> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.row_layput, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Model temp = items.get(position);
        viewHolder.itemName.setText(temp.getItemName());
        viewHolder.itemDescription.setText(temp.getItemDescription());


        return convertView;
    }

    private class ViewHolder {
        TextView itemName;
        ImageView image;
        TextView itemDescription;

        public ViewHolder(View view) {
            itemName = (TextView) view.findViewById(R.id.textView1);
            itemDescription = (TextView) view.findViewById(R.id.textView2);
            image = (ImageView) view.findViewById(R.id.imageView);

        }

    }
}
